#!/usr/bin/env bash

# Script for Setting the Database up.

export PGPASSWORD=root
dropdb mlm -h db_container -p 5432 -U postgres
createdb mlm -h db_container -p 5432 -U postgres

# Deleting all migrations
ls -d1 /mlm/mlm_tree/migrations/* | grep -v '__' | xargs -d "\n" rm

python3.6 /mlm/manage.py makemigrations
python3.6 /mlm/manage.py migrate

python3.6 /mlm/manage.py shell < /mlm/mlm_tests.py

python3.6 /mlm/manage.py createsuperuser
