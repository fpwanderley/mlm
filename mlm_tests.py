from pprint import pprint

from mlm_tree.models import MLMNode, User
from mlm_tree.utils import TreeSide, usernamize_name, emailize_name

RESTART_DB = True
DEFAULT_PASSWORD = '123456'

if RESTART_DB:
    MLMNode.objects.all().delete()
    User.objects.all().delete()

    names = ['Maricruz Geren', 'Sam Brasier', 'Yvone Dickinson',
             'Cheryle Battles', 'Drucilla Vath', 'Latesha Gregerson',
             'Charley Moring', 'Liza Dugger', 'Imelda Hsieh', 'Leeann Natali',
             'Zina Conine', 'Alfonzo Bartley', 'Broderick Cullinan', 'Isabella Ardito',
             'Annie Kells', 'Corrie Broker', 'Conchita Croxton', 'Johna Brick',
             'Delcie Barksdale', 'Elisa Stutes']

    new_test_model = User.objects.create(name=names[0],
                                         username=usernamize_name(names[0]),
                                         email=emailize_name(names[0]))
    new_test_model.set_password(DEFAULT_PASSWORD)
    new_test_model.save()

    first_node = MLMNode.objects.create(reference_object=new_test_model)

    for idx, name in enumerate(names[1:]):
        new_test_model = User.objects.create(name=name,
                                             username=usernamize_name(name),
                                             email=emailize_name(name))
        new_test_model.set_password(DEFAULT_PASSWORD)
        new_test_model.save()

        side_to_add = TreeSide.OUTER_SIDE
        if idx % 2 == 0:
            side_to_add = TreeSide.INNER_SIDE

        first_node.add_node(reference_object=new_test_model, side_to_add=side_to_add)

first_node = MLMNode.objects.get(id=1)
