#!/usr/bin/env bash

# ##############################################################
echo "Updating Python Packages"
python3.6 -m pip install -r /mlm/requirements.txt

echo "Updating MLM Tree Database"
source /mlm/setup_db.sh

echo "Starting Django Development Server"
python3.6 /mlm/manage.py runserver 0.0.0.0:8000
