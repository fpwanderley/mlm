import json

from django.shortcuts import render
from rest_framework.schemas import get_schema_view
from rest_framework_swagger.renderers import SwaggerUIRenderer, OpenAPIRenderer

from .models import MLMNode


# Create your views here.
def show_tree(request):

    mlmnode_id = request.GET.get('id')

    if mlmnode_id:
        serialized_tree = MLMNode.objects.get(id=int(mlmnode_id)).get_treant_serialized_tree()
    else:
        serialized_tree = MLMNode.objects.get(father=None).get_treant_serialized_tree()

    context = {
        'serialized_tree': json.dumps(serialized_tree)
    }

    return render(request, 'show_tree.html', context)


# View for accessing the Swagger interface.
schema_view = get_schema_view(title='MLM API', renderer_classes=[OpenAPIRenderer, SwaggerUIRenderer])

