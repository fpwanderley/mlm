# -*- coding: utf-8 -*-


def usernamize_name(raw_string):
    return raw_string.lower().replace(' ', '')


def emailize_name(raw_string):
    MLM_SUFIX = '@mlm.com'

    return '{username}@{sufix}'.format(username=usernamize_name(raw_string),
                                       sufix=MLM_SUFIX)


class TreeSide(object):

    """
        INNER SIDE: No nosso contexto, sera considerada a perna ESQUERDA.
        Essa perna é de propriedade própria no nó. Somente o dono dela
        consegue adicionar nós nesta perna;

        OUTER SIDE: No nosso contexto, sera considerada a perna DIREITA.
        Perna que não te retorna diretamente. O derramamento acontece nessa perna.

    """

    INNER_SIDE = 1
    OUTER_SIDE = 2

    SIDE_CHOICES = (
        (INNER_SIDE, 'Esquerda'),
        (OUTER_SIDE, 'Direita'),
    )

    @classmethod
    def opposite_side(cls, side):
        """
            Return the value of the opposite side.

        :param side: Integer.
        :return: Integer.
        """

        if side == cls.INNER_SIDE:
            return cls.OUTER_SIDE
        elif side == cls.OUTER_SIDE:
            return cls.INNER_SIDE
        raise Exception('Side not allowed.')
