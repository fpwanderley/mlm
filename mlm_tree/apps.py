from django.apps import AppConfig


class MlmTreeConfig(AppConfig):
    name = 'mlm_tree'
