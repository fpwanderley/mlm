# -*- coding: utf-8 -*-
"""Call Endpoints."""

from rest_framework import permissions, viewsets, status
from rest_framework.views import APIView
from rest_framework.response import Response

from mlm_tree.models import MLMNode


class MLMTreeView(APIView):
    """
        A simple APIView for viewing MLMTrees.
    """

    # api/v1/mlm_tree?node_id=''
    def get(self, request):

        node_id = request.query_params.get('node_id', None)
        if node_id:
            tree_node = MLMNode.objects.get(id=node_id)
        else:
            tree_node = MLMNode.objects.get(father=None)

        return Response(tree_node.get_treant_serialized_tree(),
                        status=status.HTTP_200_OK)
