# -*- coding: utf-8 -*-

from copy import deepcopy

from django.utils import timezone
from django.db import models
from django.contrib.auth.models import AbstractBaseUser as AuthUser
from django.contrib.auth.models import UserManager, PermissionsMixin
from django.core.exceptions import ObjectDoesNotExist

from .utils import TreeSide


# Create your models here.
class MLMNode(models.Model):

    TREE_MAX_DEPTH = 5

    reference_object = models.OneToOneField('User',
                                            related_name='node',
                                            verbose_name='Integrante')
    inner_node = models.OneToOneField('MLMNode',
                                      related_name='father_inner',
                                      blank=True,
                                      null=True,
                                      verbose_name='Nó a Esquerda')
    outer_node = models.OneToOneField('MLMNode',
                                      related_name='father_outer',
                                      blank=True,
                                      null=True,
                                      verbose_name='Nó a Direita')
    father = models.ForeignKey('MLMNode', null=True, related_name='child', blank=True)
    indicator = models.ForeignKey('MLMNode',
                                  null=True,
                                  related_name='indicated',
                                  verbose_name='Patrocinador')
    height = models.IntegerField(default=0)
    created = models.DateTimeField(auto_now_add=True, auto_now=False, null=False)

    class Meta:
        ordering = ['-height']

    def __repr__(self):
        return (u'OBJECT: {father} -> {object} ) (height={height})\n'
                u'LEFT_SIDE: {left}\n'
                u'RIGHT_SIDE: {right}\n'.format(father=self.father.reference_object if self.father else None,
                                                height=self.height,
                                                object=self.reference_object,
                                                left=self.inner_node,
                                                right=self.outer_node))

    def __str__(self):
        return str(self.reference_object.name)

    @property
    def nodes(self):
        return [self.left_node, self.right_node]

    @property
    def inner_leg_length(self):
        """
            Return the inner_leg_length.

        :return: Integer.
        """

        if self.inner_node:
            return 1 + self.inner_node.outer_leg_length

        return 0

    @property
    def outer_leg_length(self):
        """
            Return the outer_leg_length.

        :return: Integer.
        """

        if self.outer_node:
            return 1 + self.outer_node.outer_leg_length

        return 0

    @property
    def binary_qtt(self):
        """
            Return the quantity of binaries.

        :return: Integer.
        """

        return min([self.inner_leg_length, self.outer_leg_length])

    def _select_node_name(self, selector):
        """
            Return the correct node's name depending on the passed selector.

        :param selector: Integer constraint.
        :return: String.
        """

        if selector == TreeSide.INNER_SIDE:
            return 'inner_node'
        elif selector == TreeSide.OUTER_SIDE:
            return 'outer_node'

        raise Exception('Selector not allowed.')

    def add_node(self, reference_object=None, side_to_add=TreeSide.INNER_SIDE):
        """
            Add node into self's subtree.

        :param side_to_add: Boolean to decide which tree to add.
        :return: The added MLMNode object.
        """

        if side_to_add not in [TreeSide.INNER_SIDE, TreeSide.OUTER_SIDE]:
            raise Exception('Tree side not allowed.')

        self_leg_to_change = getattr(self, self._select_node_name(side_to_add))

        if self_leg_to_change is None:
            node_to_add = MLMNode.objects.create(reference_object=reference_object,
                                                 father=self)

            # Attach the new node into the tree.
            setattr(self, self._select_node_name(side_to_add), node_to_add)

            self.save()
            return node_to_add

        else:
            node_to_call = self_leg_to_change
            return node_to_call.add_node(reference_object=reference_object,
                                         side_to_add=TreeSide.OUTER_SIDE)

    def get_serialized_tree(self):
        """
            Return a Dict with the current tree serialized.

        :return: Dict.
        """

        serialized_tree = {
            'node': self.reference_object.json_object,
            'left': self.inner_node.get_serialized_tree() if self.inner_node else None,
            'right': self.outer_node.get_serialized_tree() if self.outer_node else None,
        }

        return serialized_tree

    def get_treant_serialized_tree(self):
        """
            Return a Dict with the current tree serialized for Treant JS Library.

        :return: Dict.
        """

        serialized_tree = {
            "text": {
                "name": u"{name} ({id}) [{inner}/{outer}/{binary}]".format(name=self.reference_object.name,
                                                                           id=self.id,
                                                                           inner=self.inner_leg_length,
                                                                           outer=self.outer_leg_length,
                                                                           binary=self.binary_qtt)
            }
        }

        children = []
        if self.inner_node:
            children.append(self.inner_node.get_treant_serialized_tree())
        if self.outer_node:
            children.append(self.outer_node.get_treant_serialized_tree())

        if children:
            serialized_tree.update({
                "children": children
            })

        return serialized_tree


class User(AuthUser, PermissionsMixin):

    username = models.CharField(max_length=20, unique=True)
    email = models.EmailField(unique=True)

    name = models.CharField(max_length=30)
    phone = models.CharField(max_length=50, blank=True, null=True)
    cellphone = models.CharField(max_length=50, blank=True, null=True)
    birth_date = models.DateField(blank=True, null=True)
    document_type = models.CharField(max_length=30, null=True)
    numberDocumentType = models.CharField(max_length=30, null=True)

    street = models.CharField(max_length=30, null=True)
    number = models.CharField(max_length=10, null=True)
    neighborhood = models.CharField(max_length=30, null=True)
    country = models.CharField(max_length=20, null=True)   # TODO: Colocar lista de possíves Países.
    city = models.CharField(max_length=20, null=True)   # TODO: Colocar lista de possíves Cidades.
    district = models.CharField(max_length=20, null=True)
    zip_code = models.CharField(max_length=20, null=True)

    date_joined = models.DateTimeField('date joined', default=timezone.now)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    second_password = models.CharField(max_length=50, blank=False, null=False, default='---')

    created = models.DateTimeField(auto_now_add=True, auto_now=False, null=True, verbose_name='Criado')
    updated = models.DateTimeField(auto_now_add=False, auto_now=True, null=True, verbose_name='Último acesso')

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email', 'name']

    objects = UserManager()

    class Meta:
        verbose_name = u'Usuário'

    def __str__(self):
        return str(self.name)

    @property
    def get_short_name(self):
        return self.username

    @property
    def get_tree_node(self):
        """
            Return the respective MLMNode, if it exists.

        :return: MLMNode.
        """

        try:
            tree_node = MLMNode.objects.get(reference_object=self)
            return tree_node

        except ObjectDoesNotExist as exc:
            return None

    @property
    def json_object(self):
        """
            Return a Dict with all important data to be retrieved in User's tree.

        :return: Dict.
        """

        return {
            'id': self.id,
            'name': self.name,
        }

    def get_serialized_tree(self):
        """
            Return a Dict with the User's tree.

        :return: Dict
        """

        if self.get_tree_node:
            return self.node.get_serialized_tree()

        else:
            return None


class Indication(models.Model):
    referral = models.ForeignKey(User, blank=False, null=False)
    referred = models.OneToOneField(User, blank=False, null=False, related_name='own_indication')
    indication_side = models.IntegerField(choices=TreeSide.SIDE_CHOICES, blank=False,
                                          null=False, default=TreeSide.SIDE_CHOICES[0][0])

    created = models.DateTimeField(auto_now_add=True, auto_now=False, null=True, verbose_name='Criado')
    updated = models.DateTimeField(auto_now_add=False, auto_now=True, null=True, verbose_name='Último acesso')

    class Meta:
        verbose_name = u'Indicação'


class Plan(models.Model):
    name = models.CharField(max_length=50, blank=False, null=False, verbose_name=u'Nome')
    value = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=u'Valor')
    binary_bonus = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=u'Bônus Binário')
    binary_percentage = models.DecimalField(max_digits=5, decimal_places=2, verbose_name=u'Porcentagem de Binário')
    indication_percentage = models.DecimalField(max_digits=5, decimal_places=2, verbose_name=u'Porcentagem por Indicação')
    team_formation_percentage = models.DecimalField(max_digits=5, decimal_places=2, verbose_name=u'Porcentagem por Formação de Time')
    is_active = models.BooleanField(default=True, verbose_name=u'Ativo')

    created = models.DateTimeField(auto_now_add=True, auto_now=False, null=True, verbose_name='Criado')
    updated = models.DateTimeField(auto_now_add=False, auto_now=True, null=True, verbose_name='Último acesso')

    class Meta:
        verbose_name = u'Plano'

    def save(self, *args, **kwargs):

        # If it is been edited and it will be inactivated.
        if self.pk and self.is_active:

            # Create a new Plan.
            new_plan = deepcopy(self)
            new_plan.pk = None
            new_plan.save()

            # Inactivate the old Plan.
            old_plan = Plan.objects.get(id=self.id)
            old_plan.is_active = False
            old_plan.save()

            # Updated all User with the new Plan.
            UserPlan.set_user_plan(users=self.users(),
                                   plan=new_plan)

        # It is been created.
        else:
            super(Plan, self).save(*args, **kwargs)

    def users(self):
        """
            Return all User with this Plan.

        :return: User QuerySet.
        """

        user_ids = set(self.userplan_set.all().values_list('user', flat=True))
        users = User.objects.filter(id__in=user_ids)

        return users


class UserPlan(models.Model):
    user = models.ForeignKey(User, blank=False, null=False, verbose_name=u'Usuário')
    plan = models.ForeignKey(Plan, blank=False, null=False, verbose_name=u'Plan')

    created = models.DateTimeField(auto_now_add=True, auto_now=False, null=True, verbose_name=u'Criado')

    @classmethod
    def set_user_plan(cls, users, plan):
        """
            Create an User Plan for each user in users with the given Plan.

        :param users: User QuerySet.
        :param plan: Plan obj.
        :return: None.
        """

        if plan:
            for user in users:
                cls.objects.create(user=user,
                                   plan=plan)
