# -*- coding: utf-8 -*-

from django import forms

from django.contrib import admin
from django.http import HttpResponseRedirect

from .models import MLMNode, User, Plan
from .utils import TreeSide

# Register your models here.


class MLMNodeForm(forms.ModelForm):

    tree_side = forms.ChoiceField(choices=TreeSide.SIDE_CHOICES,
                                  label='Lado da Árvore')

    class Meta:
        model = MLMNode
        fields = ('reference_object', 'indicator')


class MLMNodeAdmin(admin.ModelAdmin):
    form = MLMNodeForm
    list_display = ('id', 'reference_object', 'indicator', 'inner_node', 'outer_node', 'get_binary_qtt')

    class Meta:
        model = MLMNode

    def save_model(self, request, obj, form, change):
        father = obj.indicator
        side_to_add = int(form.data.get('tree_side'))
        new_node = father.add_node(reference_object=obj.reference_object,
                                   side_to_add=side_to_add)
        new_node.indicator = father
        new_node.save()

    def get_binary_qtt(self, obj):
        return obj.binary_qtt
    get_binary_qtt.short_description = u'Número de Binários'


class UserModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'username', 'email')

    class Meta:
        model = User


class PlanModelAdmin(admin.ModelAdmin):
    list_display = ('name', 'value', 'binary_bonus', 'binary_percentage', 'indication_percentage',
                    'team_formation_percentage', 'is_active', 'created')

    class Meta:
        model = Plan

admin.site.register(MLMNode, MLMNodeAdmin)
admin.site.register(User, UserModelAdmin)
admin.site.register(Plan, PlanModelAdmin)
