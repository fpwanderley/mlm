# -*- coding: utf-8 -*-
"""Call Endpoints."""

from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from mlm_tree.models import Plan
from mlm_tree.plans.serializers import PlanSerializer


class PlanViewSet(viewsets.ModelViewSet):
    """
        A simple ViewSet for viewing creating, editing and retrieving PLANS.

    """

    permission_classes = (IsAuthenticated,)

    queryset = Plan.objects.all()
    serializer_class = PlanSerializer
