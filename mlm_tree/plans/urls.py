# -*- coding: utf-8 -*-

from rest_framework import routers

from mlm_tree.plans.endpoints import PlanViewSet


plan_router = routers.SimpleRouter(trailing_slash=False)
plan_router.register(r'plans', PlanViewSet, 'Plan')
