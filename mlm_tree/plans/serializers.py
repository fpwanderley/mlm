# -*- coding: utf-8 -*-


from rest_framework import serializers

from mlm_tree.models import Plan


class PlanSerializer(serializers.ModelSerializer):

    class Meta:
        model = Plan
        fields = ('id', 'name', 'value', 'binary_bonus', 'binary_percentage',
                  'indication_percentage', 'team_formation_percentage', 'is_active')
