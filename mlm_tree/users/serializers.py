# -*- coding: utf-8 -*-

from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction

from rest_framework import serializers

from mlm_tree.models import User, Indication, MLMNode


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'name', 'username', 'email', 'password', 'second_password',
                  'street', 'number', 'neighborhood', 'country', 'city', 'district', 'zip_code',
                  'phone', 'cellphone', 'birth_date', 'document_type', 'numberDocumentType')
        extra_kwargs = {
            'password': {'write_only': True},
            'second_password': {'write_only': True}
        }

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance

    def update(self, instance, validated_data):
        for attr, value in validated_data.items():
            if attr == 'password':
                instance.set_password(value)
            else:
                setattr(instance, attr, value)
        instance.save()
        return instance


class IndicationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Indication
        fields = ('referral', 'referred', 'indication_side')

    def validate_referral(self, value):
        referral_tree_node = value.get_tree_node
        if referral_tree_node:
            return value

        raise serializers.ValidationError(u"{user} ainda não tem posição na árvore. "
                                          u"Adicione-o na árvore primeiro.".format(user=value.name))

    def validate_referred(self, value):
        referral_tree_node = value.get_tree_node
        if referral_tree_node:
            raise serializers.ValidationError(u"{user} ainda já possui posição na Árvore. "
                                              u"Não é possível adicioná-lo novamente.".format(user=value.name))
        return value

    @transaction.atomic
    def create(self, validated_data):

        # Create an Indication obj.
        instance = self.Meta.model(**validated_data)
        instance.save()

        # Add the referred in the Tree.
        referral = instance.referral
        referred = instance.referred
        indication_side = instance.indication_side
        referral.get_tree_node.add_node(reference_object=referred, side_to_add=indication_side)

        return instance
