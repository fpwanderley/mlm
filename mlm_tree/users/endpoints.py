# -*- coding: utf-8 -*-
"""Call Endpoints."""

from rest_framework import permissions, viewsets, status
from rest_framework.views import APIView
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny

from mlm_tree.models import User, Indication
from mlm_tree.users.serializers import UserSerializer, IndicationSerializer


class LoginViewSet(APIView):

    def get(self, request, format=None):
        """
            Return the User token.
        """
        token, _ = Token.objects.get_or_create(user=request.user)
        data = {
            'user_email': request.user.email,
            'token': token.key,
            'id': request.user.id
        }
        return Response(data=data)


class UserViewSet(viewsets.ModelViewSet):
    """
        A simple ViewSet for viewing creating and editing Users.

    """

    permission_classes = (AllowAny,)

    queryset = User.objects.all()
    serializer_class = UserSerializer


class IndicationViewSet(viewsets.ModelViewSet):
    """
        A simple ViewSet for viewing creating and editing Indication.(LEFT_SIDE = 1 (Default), RIGHT_SIDE = 2)

    """

    permission_classes = (IsAuthenticated,)

    queryset = Indication.objects.all()
    serializer_class = IndicationSerializer


class GetUserTree(APIView):

    def get(self, request, format=None):
        """
            Return the User's MLM Tree (?user_id=<id>).
        """

        user_id = request.query_params.get('user_id')

        if user_id:

            try:
                user = User.objects.get(id=user_id)
                user_tree = user.get_serialized_tree()

                if user_tree:
                    return Response(data=user_tree, status=status.HTTP_200_OK)

                else:
                    return Response(data=u'User does not have a tree', status=status.HTTP_404_NOT_FOUND)

            except User.DoesNotExist:
                return Response(data=u'Invalid user_id.', status=status.HTTP_400_BAD_REQUEST)

        else:
            return Response(data=u'An user_id has to be sent.', status=status.HTTP_400_BAD_REQUEST)


