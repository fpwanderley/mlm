# -*- coding: utf-8 -*-

from django.conf.urls import url

from rest_framework import routers

from mlm_tree.users.endpoints import LoginViewSet, UserViewSet, IndicationViewSet, GetUserTree


user_router = routers.SimpleRouter(trailing_slash=False)
user_router.register(r'users', UserViewSet)

indication_router = routers.SimpleRouter(trailing_slash=False)
indication_router.register(r'indication', IndicationViewSet)

login_url = [
    url(r'^login', LoginViewSet.as_view(), name='login')
]

user_tree_url = [
    url(r'^user_tree', GetUserTree.as_view(), name='user_tree')
]
