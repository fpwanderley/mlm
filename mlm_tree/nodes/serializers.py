# -*- coding: utf-8 -*-

from rest_framework import serializers
from rest_framework_recursive.fields import RecursiveField

from mlm_tree.models import MLMNode


class MLMNodeSerializer(serializers.ModelSerializer):

    inner_node = RecursiveField(allow_null=True)
    outer_node = RecursiveField(allow_null=True)

    class Meta:
        model = MLMNode
        fields = ('id', 'reference_object', 'inner_node', 'outer_node')

