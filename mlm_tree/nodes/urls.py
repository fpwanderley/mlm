# -*- coding: utf-8 -*-

from rest_framework import routers

from mlm_tree.nodes.endpoints import MLMNodeViewSet


node_router = routers.SimpleRouter(trailing_slash=False)
node_router.register(r'nodes', MLMNodeViewSet, 'MLMNode')
