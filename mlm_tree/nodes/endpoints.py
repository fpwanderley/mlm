# -*- coding: utf-8 -*-
"""Call Endpoints."""

from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated

from mlm_tree.models import MLMNode
from mlm_tree.nodes.serializers import MLMNodeSerializer


class MLMNodeViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for viewing creating and editing MLMNodes

    Filter fields such as default, name and group are available for search.
    """

    permission_classes = (IsAuthenticated,)
    serializer_class = MLMNodeSerializer

    def get_queryset(self):

        queryset = MLMNode.objects.all()
        id = self.request.query_params.get('id', None)
        if id is not None:
            queryset = queryset.filter(id=id)
        return queryset
