# Tutorial de Montagem do Ambiente:
1. Rodar o script para instalação do Docker e criação das imagens Docker com os ambientes do MLM Tree:
> source setup.sh
2. No arquivo run_images,  atualizar a variável SOURCE com o path da pasta do projeto;
Executar os containers docker necessários para subir o ambiente:
> source run_images.sh -i envs -v 1.0 -c /bin/bash
3. Ao executar o comando acima, você vai estar executando dentro de um container docker. Teu terminal estará parecido com algo do tipo
> **root@f25ecb32e9df:/#**
4. Inicializar o banco de dados da aplicação:
4.1 *setup_db* é um alias que já cria o banco, as migrações e executa um script pra popular a base inicialmente para testes;
> **setup_db**
5. Subir a aplicação com o servidor de desenvolvimento:
5.1 *runserver* é um alias que sobe a aplicação na porta 8000.
> runserver
6. Acesse a aplicação:
> localhost:8000/admin

# Tutorial Deploying App into Heroku:
1. Sincronizar os repositórios:
> git push heroku master
2. Abrir o App no Browser:
> heroku open
3. Reiniciar o DB:
> heroku pg:reset DATABASE
4. Rodar as migrações no DB:
> heroku run python manage.py migrate
5. Rodando o script inicial para povoar o banco:
> heroku run python manage.py shell < mlm_tests.py
6. Criando um novo superuser:
> heroku run python manage.py createsuperuser
7. SuperUsuário de acesso:
    - **User:** user
    - **Password:** mlmpassword
8. URL no Heroku:
    - https://quiet-caverns-91552.herokuapp.com/#/

# Não commitar essas libs Python no requirements.txt:
- pygobject==3.20.0
- python-apt==1.1.0b1
- unattended-upgrades==0.1

# URLs Úteis:
1. Recuperar o Token de login do usuário:
    - https://quiet-caverns-91552.herokuapp.com/api/v1/login
    - **User:** user
    - **Password:** mlmpassword
    - **OBS**: Incluir o header ('Authorization', 'Token <token retornado no login>')
2. Mostrar uma representação gráfica da árvore no sistema:
    - https://quiet-caverns-91552.herokuapp.com/show_tree
