"""mlm URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin

from mlm_tree.mlm_tree.endpoints import MLMTreeView
from mlm_tree.views import show_tree, schema_view
from mlm_tree.users.urls import user_router, login_url, indication_router, user_tree_url
from mlm_tree.nodes.urls import node_router
from mlm_tree.plans.urls import plan_router


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/v1/mlm_tree',
        MLMTreeView.as_view(), name='mlm-tree'),
    url(r'^show_tree/', show_tree),
    url(r'^api/v1/', include(user_router.urls)),
    url(r'^api/v1/', include(node_router.urls)),
    url(r'^api/v1/', include(indication_router.urls)),
    url(r'^api/v1/', include(plan_router.urls)),
    url(r'^api/v1/', include(login_url)),
    url(r'^api/v1/', include(user_tree_url)),
    url(r'^$', schema_view),
]
